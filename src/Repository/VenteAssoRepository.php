<?php

namespace App\Repository;

use App\Entity\VenteAsso;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method VenteAsso|null find($id, $lockMode = null, $lockVersion = null)
 * @method VenteAsso|null findOneBy(array $criteria, array $orderBy = null)
 * @method VenteAsso[]    findAll()
 * @method VenteAsso[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VenteAssoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VenteAsso::class);
    }

    // /**
    //  * @return VenteAsso[] Returns an array of VenteAsso objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?VenteAsso
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
