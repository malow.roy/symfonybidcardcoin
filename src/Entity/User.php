<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\Column(type="integer")
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $addresse;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAcheteur;

    /**
     * @ORM\ManyToMany(targetEntity=Vente::class, mappedBy="participants")
     */
    private $ventes;

    /**
     * @ORM\OneToMany(targetEntity=Produit::class, mappedBy="idVendeur")
     */
    private $produitsAsVendeur;

    /**
     * @ORM\OneToMany(targetEntity=Enchere::class, mappedBy="idAcheteur")
     */
    private $encheres;

    public function __construct()
    {
        $this->ventes = new ArrayCollection();
        $this->produitsAsVendeur = new ArrayCollection();
        $this->encheres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getTelephone(): ?int
    {
        return $this->telephone;
    }

    public function setTelephone(int $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }


    public function setPassword(string $password): self
    {
        $this->password = $password;
        print($this->password);
        return $this;
    }

    public function getAddresse(): ?string
    {
        return $this->addresse;
    }

    public function setAddresse(string $addresse): self
    {
        $this->addresse = $addresse;

        return $this;
    }

    public function getIsAcheteur(): ?bool
    {
        return $this->isAcheteur;
    }

    public function setIsAcheteur(bool $isAcheteur): self
    {
        $this->isAcheteur = $isAcheteur;

        return $this;
    }

    /**
     * @return Collection|Vente[]
     */
    public function getVentes(): Collection
    {
        return $this->ventes;
    }

    public function addVente(Vente $vente): self
    {
        if (!$this->ventes->contains($vente)) {
            $this->ventes[] = $vente;
            $vente->addParticipant($this);
        }

        return $this;
    }

    public function removeVente(Vente $vente): self
    {
        if ($this->ventes->removeElement($vente)) {
            $vente->removeParticipant($this);
        }

        return $this;
    }

    /**
     * @return Collection|Produit[]
     */
    public function getProduitsAsVendeur(): Collection
    {
        return $this->produitsAsVendeur;
    }

    public function addProduitsAsVendeur(Produit $produitsAsVendeur): self
    {
        if (!$this->produitsAsVendeur->contains($produitsAsVendeur)) {
            $this->produitsAsVendeur[] = $produitsAsVendeur;
            $produitsAsVendeur->setIdVendeur($this);
        }

        return $this;
    }

    public function removeProduitsAsVendeur(Produit $produitsAsVendeur): self
    {
        if ($this->produitsAsVendeur->removeElement($produitsAsVendeur)) {
            // set the owning side to null (unless already changed)
            if ($produitsAsVendeur->getIdVendeur() === $this) {
                $produitsAsVendeur->setIdVendeur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Enchere[]
     */
    public function getEncheres(): Collection
    {
        return $this->encheres;
    }

    public function addEnchere(Enchere $enchere): self
    {
        if (!$this->encheres->contains($enchere)) {
            $this->encheres[] = $enchere;
            $enchere->setIdAcheteur($this);
        }

        return $this;
    }

    public function removeEnchere(Enchere $enchere): self
    {
        if ($this->encheres->removeElement($enchere)) {
            // set the owning side to null (unless already changed)
            if ($enchere->getIdAcheteur() === $this) {
                $enchere->setIdAcheteur(null);
            }
        }

        return $this;
    }

    public function getRoles()
    {
        $roles[] = 'ROLE_USER';
        return array_unique($roles);
    }

    public function getPassword()
    {
        return (string) $this->password;
    }

    public function getSalt()
    {
    }

    public function getUsername()
    {
        return (string) $this->email;
    }

    public function eraseCredentials()
    {
    }
}
