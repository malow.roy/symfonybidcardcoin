<?php

namespace App\Entity;

use App\Repository\EnchereRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EnchereRepository::class)
 */
class Enchere
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $prix;

    /**
     * @ORM\ManyToOne(targetEntity=Vente::class, inversedBy="encheres")
     * @ORM\JoinColumn(nullable=false)
     */
    private $idVente;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="encheres")
     * @ORM\JoinColumn(nullable=false)
     */
    private $idAcheteur;

    /**
     * @ORM\ManyToOne(targetEntity=Produit::class, inversedBy="encheres")
     */
    private $idProduit;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getIdVente(): ?Vente
    {
        return $this->idVente;
    }

    public function setIdVente(?Vente $idVente): self
    {
        $this->idVente = $idVente;

        return $this;
    }

    public function getIdAcheteur(): ?User
    {
        return $this->idAcheteur;
    }

    public function setIdAcheteur(?User $idAcheteur): self
    {
        $this->idAcheteur = $idAcheteur;

        return $this;
    }

    public function getIdProduit(): ?Produit
    {
        return $this->idProduit;
    }

    public function setIdProduit(?Produit $idProduit): self
    {
        $this->idProduit = $idProduit;

        return $this;
    }

}
