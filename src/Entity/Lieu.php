<?php

namespace App\Entity;

use App\Repository\LieuRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LieuRepository::class)
 */
class Lieu
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $postalCode;

    /**
     * @ORM\OneToMany(targetEntity=Produit::class, mappedBy="idLieu")
     */
    private $stockedProduits;

    /**
     * @ORM\OneToMany(targetEntity=Vente::class, mappedBy="idLieu")
     */
    private $ventes;

    public function __construct()
    {
        $this->stockedProduits = new ArrayCollection();
        $this->ventes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * @return Collection|Produit[]
     */
    public function getStockedProduits(): Collection
    {
        return $this->stockedProduits;
    }

    public function addStockedProduit(Produit $stockedProduit): self
    {
        if (!$this->stockedProduits->contains($stockedProduit)) {
            $this->stockedProduits[] = $stockedProduit;
            $stockedProduit->setIdLieu($this);
        }

        return $this;
    }

    public function removeStockedProduit(Produit $stockedProduit): self
    {
        if ($this->stockedProduits->removeElement($stockedProduit)) {
            // set the owning side to null (unless already changed)
            if ($stockedProduit->getIdLieu() === $this) {
                $stockedProduit->setIdLieu(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Vente[]
     */
    public function getVentes(): Collection
    {
        return $this->ventes;
    }

    public function addVente(Vente $vente): self
    {
        if (!$this->ventes->contains($vente)) {
            $this->ventes[] = $vente;
            $vente->setIdLieu($this);
        }

        return $this;
    }

    public function removeVente(Vente $vente): self
    {
        if ($this->ventes->removeElement($vente)) {
            // set the owning side to null (unless already changed)
            if ($vente->getIdLieu() === $this) {
                $vente->setIdLieu(null);
            }
        }

        return $this;
    }
}
