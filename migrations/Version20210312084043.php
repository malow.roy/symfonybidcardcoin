<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210312084043 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE categorie (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE enchere (id INT AUTO_INCREMENT NOT NULL, id_vente_id INT NOT NULL, id_acheteur_id INT NOT NULL, id_produit_id INT DEFAULT NULL, prix DOUBLE PRECISION NOT NULL, INDEX IDX_38D1870F2D1CFB9F (id_vente_id), INDEX IDX_38D1870F8EB576A8 (id_acheteur_id), INDEX IDX_38D1870FAABEFE2C (id_produit_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lieu (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, ville VARCHAR(255) NOT NULL, adresse VARCHAR(255) NOT NULL, postal_code VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lot (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, id_vente INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE produit (id INT AUTO_INCREMENT NOT NULL, id_vendeur_id INT NOT NULL, id_categorie_id INT NOT NULL, id_lot_id INT DEFAULT NULL, id_lieu_id INT DEFAULT NULL, titre VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, volume DOUBLE PRECISION NOT NULL, estimation INT NOT NULL, poids DOUBLE PRECISION NOT NULL, materiau VARCHAR(255) NOT NULL, INDEX IDX_29A5EC2720068689 (id_vendeur_id), INDEX IDX_29A5EC279F34925F (id_categorie_id), INDEX IDX_29A5EC278EFC101A (id_lot_id), INDEX IDX_29A5EC27B42FBABC (id_lieu_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `user` (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, telephone INT NOT NULL, email VARCHAR(255) NOT NULL, mdp VARCHAR(255) NOT NULL, addresse VARCHAR(255) NOT NULL, is_acheteur TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vente (id INT AUTO_INCREMENT NOT NULL, id_lieu_id INT DEFAULT NULL, date DATE NOT NULL, titre VARCHAR(255) NOT NULL, is_finished TINYINT(1) NOT NULL, INDEX IDX_888A2A4CB42FBABC (id_lieu_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vente_user (vente_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_9DC2D0557DC7170A (vente_id), INDEX IDX_9DC2D055A76ED395 (user_id), PRIMARY KEY(vente_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE enchere ADD CONSTRAINT FK_38D1870F2D1CFB9F FOREIGN KEY (id_vente_id) REFERENCES vente (id)');
        $this->addSql('ALTER TABLE enchere ADD CONSTRAINT FK_38D1870F8EB576A8 FOREIGN KEY (id_acheteur_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE enchere ADD CONSTRAINT FK_38D1870FAABEFE2C FOREIGN KEY (id_produit_id) REFERENCES produit (id)');
        $this->addSql('ALTER TABLE produit ADD CONSTRAINT FK_29A5EC2720068689 FOREIGN KEY (id_vendeur_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE produit ADD CONSTRAINT FK_29A5EC279F34925F FOREIGN KEY (id_categorie_id) REFERENCES categorie (id)');
        $this->addSql('ALTER TABLE produit ADD CONSTRAINT FK_29A5EC278EFC101A FOREIGN KEY (id_lot_id) REFERENCES lot (id)');
        $this->addSql('ALTER TABLE produit ADD CONSTRAINT FK_29A5EC27B42FBABC FOREIGN KEY (id_lieu_id) REFERENCES lieu (id)');
        $this->addSql('ALTER TABLE vente ADD CONSTRAINT FK_888A2A4CB42FBABC FOREIGN KEY (id_lieu_id) REFERENCES lieu (id)');
        $this->addSql('ALTER TABLE vente_user ADD CONSTRAINT FK_9DC2D0557DC7170A FOREIGN KEY (vente_id) REFERENCES vente (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE vente_user ADD CONSTRAINT FK_9DC2D055A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE produit DROP FOREIGN KEY FK_29A5EC279F34925F');
        $this->addSql('ALTER TABLE produit DROP FOREIGN KEY FK_29A5EC27B42FBABC');
        $this->addSql('ALTER TABLE vente DROP FOREIGN KEY FK_888A2A4CB42FBABC');
        $this->addSql('ALTER TABLE produit DROP FOREIGN KEY FK_29A5EC278EFC101A');
        $this->addSql('ALTER TABLE enchere DROP FOREIGN KEY FK_38D1870FAABEFE2C');
        $this->addSql('ALTER TABLE enchere DROP FOREIGN KEY FK_38D1870F8EB576A8');
        $this->addSql('ALTER TABLE produit DROP FOREIGN KEY FK_29A5EC2720068689');
        $this->addSql('ALTER TABLE vente_user DROP FOREIGN KEY FK_9DC2D055A76ED395');
        $this->addSql('ALTER TABLE enchere DROP FOREIGN KEY FK_38D1870F2D1CFB9F');
        $this->addSql('ALTER TABLE vente_user DROP FOREIGN KEY FK_9DC2D0557DC7170A');
        $this->addSql('DROP TABLE categorie');
        $this->addSql('DROP TABLE enchere');
        $this->addSql('DROP TABLE lieu');
        $this->addSql('DROP TABLE lot');
        $this->addSql('DROP TABLE produit');
        $this->addSql('DROP TABLE `user`');
        $this->addSql('DROP TABLE vente');
        $this->addSql('DROP TABLE vente_user');
    }
}
